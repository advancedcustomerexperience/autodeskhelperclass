﻿Imports Autodesk.AutoCAD.Interop
Imports System.Text.RegularExpressions
Imports System.IO
Imports NativeExcel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports System.Xml

Public Class autodeskHelperClass
    Implements IDisposable

    Public blnQuitInventor As Boolean = True
    Public blnQuitMdt As Boolean = True
    Public blnSilentOperation As Boolean = False ' True
    Private strProgId As String = "AutoCAD.Application"

    Public Sub New(ByVal blnInventor As Boolean, ByVal blnMdt As Boolean, blnAcad As Boolean)
        'If blnInventor = True Then getInventorApp()
        If blnMdt = True Then getMdtApp()
        If blnAcad = True Then getAcadApp()
    End Sub

    Private Sub loadAcad()
        'Shell("C:\Program Files\Autodesk\MDT 2008\acad.exe" & " /p <<VANILLA>>") '& " /p <<ACADMPP>>")
        'Shell(" C:\Program Files\Autodesk\ACADM 2011\acad.exe") ' & " /p <<VANILLA>>") '& " /p <<ACADMPP>>")
        Threading.Thread.Sleep(5000)
        Try
            nMdtApp = New AcadApplication
        Catch ex As Exception
            Threading.Thread.Sleep(9000)
            Try
                nMdtApp = New AcadApplication
            Catch ex1 As Exception
                Threading.Thread.Sleep(9000)
                Try
                    nMdtApp = New AcadApplication 'GetObject(, strProgId)
                Catch ex2 As Exception
                    Threading.Thread.Sleep(9000)
                    nMdtApp = New AcadApplication 'GetObject(, strProgId)
                End Try
            End Try
        End Try
        blnQuitMdt = True
    End Sub

    'Private Sub loadMdt()
    '    'Shell("C:\Program Files\Autodesk\MDT 2008\acad.exe" & " /p <<MCADPP>>")
    '    Shell(" C:\Program Files\Autodesk\ACADM 2011\acad.exe" & " /p <<ACADMPP>>")
    '    Threading.Thread.Sleep(5000)
    '    Try
    '        nMdtApp = GetObject(, strProgId)
    '    Catch ex As Exception
    '        Threading.Thread.Sleep(9000)
    '        Try
    '            nMdtApp = GetObject(, strProgId)
    '        Catch ex1 As Exception
    '            Threading.Thread.Sleep(90000)
    '            Try
    '                nMdtApp = GetObject(, strProgId)
    '            Catch ex2 As Exception
    '                Threading.Thread.Sleep(20000)
    '                nMdtApp = GetObject(, strProgId)
    '            End Try
    '        End Try
    '    End Try
    '    blnQuitMdt = True
    'End Sub

    'Private nInventorApp As Inventor.Application
    'Public Property InventorApp() As Inventor.Application
    '    Get
    '        Return nInventorApp
    '    End Get
    '    Set(ByVal value As Inventor.Application)
    '        nInventorApp = value
    '    End Set
    'End Property

    Private nMdtApp As Autodesk.AutoCAD.Interop.AcadApplication
    Public Property MdtApp() As Autodesk.AutoCAD.Interop.AcadApplication
        Get
            Return nMdtApp
        End Get
        Set(ByVal value As Autodesk.AutoCAD.Interop.AcadApplication)
            nMdtApp = value
        End Set
    End Property

    Public Function MdtMemoryUsage() As Int64
        Return ProcessMemoryUsage("Acad")
    End Function

    Public Function InventorMemoryUsage() As Int64
        Return ProcessMemoryUsage("Inventor")
    End Function

    Private Function ProcessMemoryUsage(ByVal processName As String) As Int64
        ' GetProcessesByName(), our victim is Notepad... 
        Dim processes() As Process = Process.GetProcessesByName(processName)
        ' Found something? 
        If processes.Length > 0 Then
            Dim cnt As Integer
            ' Loop 
            For cnt = 0 To processes.Length - 1
                ' Process is still running? 
                If Not processes(cnt).HasExited Then
                    ' WorkingSet64: The amount of physical memory, in bytes, allocated for the associated process 
                    Return processes(cnt).WorkingSet64 / 1024
                End If ' If not 
            Next 'For cnt 
        End If ' If processes.Length > 0 Then 
        Return 0
    End Function

    Public Shared Function KillAcad() As Boolean
        ' GetProcessesByName()
        Dim processes() As Process = Process.GetProcessesByName("acad")
        ' Found something? 
        If processes.Length > 0 Then
            Dim cnt As Integer
            ' Loop 
            For cnt = 0 To processes.Length - 1
                ' Process is still running? 
                If Not processes(cnt).HasExited Then
                    For Each proc As Process In processes
                        proc.Kill()
                    Next
                    Return True
                End If ' If not 
            Next 'For cnt 
        End If ' If processes.Length > 0 Then 
        Return False
    End Function

    Public Shared Function KillInventor() As Boolean
        ' GetProcessesByName()
        Dim processes() As Process = Process.GetProcessesByName("Inventor")
        ' Found something? 
        If processes.Length > 0 Then
            Dim cnt As Integer
            ' Loop 
            For cnt = 0 To processes.Length - 1
                ' Process is still running? 
                If Not processes(cnt).HasExited Then
                    For Each proc As Process In processes
                        proc.Kill()
                    Next
                    Return True
                End If ' If not 
            Next 'For cnt 
        End If ' If processes.Length > 0 Then 
        Return False
    End Function

    Private Function KillProcess(ByVal processName As String) As Boolean
        ' GetProcessesByName()
        Dim processes() As Process = Process.GetProcessesByName(processName)
        ' Found something? 
        If processes.Length > 0 Then
            Dim cnt As Integer
            ' Loop 
            For cnt = 0 To processes.Length - 1
                ' Process is still running? 
                If Not processes(cnt).HasExited Then
                    For Each proc As Process In processes
                        proc.Kill()
                    Next
                    Return True
                End If ' If not 
            Next 'For cnt 
        End If ' If processes.Length > 0 Then 
        Return False
    End Function

    'Private Sub getInventorApp()
    '    Try
    '        nInventorApp = System.Runtime.InteropServices.Marshal.GetActiveObject("Inventor.Application")
    '        nInventorApp.SilentOperation = blnSilentOperation
    '        blnQuitInventor = False
    '    Catch ex As Exception
    '    End Try
    '    Try
    '        ' If not active, create a new Inventor session
    '        If nInventorApp Is Nothing Then
    '            Dim inventorAppType As Type = System.Type.GetTypeFromProgID("Inventor.Application")
    '            nInventorApp = System.Activator.CreateInstance(inventorAppType)
    '            nInventorApp.SilentOperation = blnSilentOperation
    '            blnQuitInventor = True
    '        End If
    '        nInventorApp.Visible = True
    '    Catch ex As Exception
    '        'MsgBox("There was a problem getting some Inventor information", "Error")
    '        If Not nInventorApp Is Nothing AndAlso blnQuitInventor = True Then
    '            Try
    '                nInventorApp.Quit()
    '            Catch ex1 As Exception
    '            End Try
    '        End If
    '        nInventorApp = Nothing
    '    End Try
    'End Sub

    'Private Sub closeInventorApp()
    '    If Not nInventorApp Is Nothing AndAlso blnQuitInventor = True Then
    '        nInventorApp.Documents.CloseAll()
    '        nInventorApp.Quit()
    '        KillProcess("Inventor")
    '    End If
    '    nInventorApp = Nothing
    'End Sub

    Private Sub getAcadApp()
        Try
            ' Get a running instance of AutoCAD 
            nMdtApp = GetObject(, strProgId)
            nMdtApp.Visible = True
            blnQuitMdt = False
        Catch ex As Exception
            ' An error occurs if no instance is running
            loadAcad()
        End Try
    End Sub

    Private Sub getMdtApp()
        Try
            ' Get a running instance of AutoCAD  
            nMdtApp = GetObject(, strProgId)
            nMdtApp.Visible = True
            blnQuitMdt = False
        Catch ex As Exception
            ' An error occurs if no instance is running 
            loadAcad()     'loadMdt()
        End Try
    End Sub

    Private Sub closeMdtApp()
        Try
            Try
                'reset variables
                If Not nMdtApp.ActiveDocument Is Nothing Then
                    nMdtApp.ActiveDocument.SetVariable("filedia", 1)
                End If
            Catch ex As Exception
            End Try
            If blnQuitMdt = True Then
                If Not nMdtApp Is Nothing Then
                    For Each doc As Autodesk.AutoCAD.Interop.AcadDocument In nMdtApp.Documents
                        doc.Close(False)
                    Next
                    nMdtApp.Quit()
                    nMdtApp = Nothing
                    KillProcess("ACAD")
                End If
            End If
        Catch ex As Exception
        Finally
            blnQuitMdt = True
        End Try
    End Sub

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If
            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
            closeMdtApp()
            'closeInventorApp()
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class

<Serializable()> _
Public Class FileDescriptions
    Inherits List(Of FileDescription)

    Public Sub New(ByVal aRootFolder As String, ByVal aNodeFont As Font)
        nRootFolder = aRootFolder
        nNodeFont = aNodeFont
    End Sub

    Private nRootFolder As String
    Public Property RootFolder() As String
        Get
            Return nRootFolder
        End Get
        Set(ByVal value As String)
            nRootFolder = value
        End Set
    End Property

    Private nNodeFont As Font
    Public Property NodeFont() As Font
        Get
            Return nNodeFont
        End Get
        Set(ByVal value As Font)
            nNodeFont = value
        End Set
    End Property

    Public Function hasFileName(ByVal aFileName) As FileDescription
        Dim qry = From f In Me Where f.FileName = aFileName Select f
        Return qry(0)
    End Function

    Public Function getTreeNodes() As TreeNode
        Dim tNode As New TreeNode("Folder Hierarchy for " & nRootFolder)
        For Each aFileDesc As FileDescription In Me
            Dim aNode As TreeNode = tNode.Nodes.Add("root", aFileDesc.FileName)
            Dim fileInfoCount As Integer = 0
            For Each aFileInfo As FileInfo In aFileDesc.FileInfo
                Dim fileInfoNode As TreeNode = aNode.Nodes.Add("fileInfo", aFileInfo.DirectoryName)
                fileInfoNode.BackColor = Drawing.Color.LightBlue
                Try
                    For Each aRefDoc As RefDoc In aFileDesc.InventorInfos(fileInfoCount).RefDocs
                        Dim imgIndex As Integer = binaryStringToInt(aRefDoc.ImageBitString)
                        Dim refDocNode2 As TreeNode
                        If imgIndex = 0 Then
                            refDocNode2 = fileInfoNode.Nodes.Add("inventorInfo", aRefDoc.Name & " - " & aRefDoc.Path)
                        Else
                            refDocNode2 = fileInfoNode.Nodes.Add("inventorInfo", aRefDoc.Name & " - " & aRefDoc.Path, imgIndex)
                        End If
                        refDocNode2.BackColor = Drawing.Color.Cornsilk
                    Next
                    fileInfoCount += 1
                Catch ex As Exception
                End Try
            Next
        Next
        Return tNode
    End Function

    Private Function binaryStringToInt(ByVal binary As String) As Integer
        Select Case binary
            Case "00000"
                Return 0
            Case "00001"
                Return 1
            Case "00010"
                Return 2
            Case "00011"
                Return 3
            Case "00100"
                Return 4
            Case "00101"
                Return 5
            Case "00110"
                Return 6
            Case "00111"
                Return 7
            Case "01000"
                Return 8
            Case "01001"
                Return 9
            Case "01010"
                Return 10
            Case "01011"
                Return 11
            Case "01100"
                Return 12
            Case "01101"
                Return 13
            Case "01110"
                Return 14
            Case "01111"
                Return 15
            Case "10000"
                Return 16
            Case "10001"
                Return 17
            Case "10010"
                Return 18
            Case "10011"
                Return 19
            Case "10100"
                Return 20
            Case "10101"
                Return 21
            Case "10110"
                Return 22
            Case "10111"
                Return 23
            Case "11000"
                Return 24
            Case "11001"
                Return 25
            Case "11010"
                Return 26
            Case "11011"
                Return 27
            Case "11100"
                Return 28
            Case "11101"
                Return 29
            Case "11110"
                Return 30
            Case "11111"
                Return 31
            Case Else
                Return 0
        End Select
    End Function

    Public Function Save(ByVal fileName As String, ByVal aFileDescriptions As FileDescriptions) As Boolean
        Try
            SerializeToFile(fileName, aFileDescriptions)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Restore(ByVal fileName As String) As FileDescriptions
        Try
            Dim aFileDescs As FileDescriptions = DeserializeFromFile(Of FileDescriptions)(fileName)
            aFileDescs.nNodeFont = nNodeFont
            Return aFileDescs
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub SerializeToFile(Of T)(ByVal path As String, ByVal obj As T)
        Using fs As New FileStream(path, FileMode.Create)
            Dim bf As New BinaryFormatter(Nothing, New StreamingContext(StreamingContextStates.File))
            bf.Serialize(fs, obj)
        End Using
    End Sub

    Public Function DeserializeFromFile(Of T)(ByVal path As String) As T
        Using fs As New FileStream(path, FileMode.Open)
            Dim bf As New BinaryFormatter(Nothing, New StreamingContext(StreamingContextStates.File))
            fs.Position = 0
            Return DirectCast(bf.Deserialize(fs), T)
        End Using
    End Function

    Public Shared Sub SerializeTreeNode(ByVal treeNode As TreeNodeCollection, ByVal fileName As String)
        Dim textWriter As XmlTextWriter = New XmlTextWriter(fileName, System.Text.Encoding.ASCII)
        ' writing the xml declaration tag
        textWriter.WriteStartDocument()
        ' writing the main tag that encloses all node tags
        textWriter.WriteStartElement("TreeView")
        ' save the nodes, recursive method
        SaveNodes(treeNode, textWriter)
        textWriter.WriteEndElement()
        textWriter.Close()
    End Sub

    Private Const XmlNodeTag As String = "node"
    Private Const XmlNodeTextAtt As String = "text"
    Private Const XmlNodeTagAtt As String = "tag"
    Private Const XmlNodeNameAtt As String = "name"
    Private Const XmlNodeForeColorAtt As String = "foreColor"
    Private Const XmlNodeBackColorAtt As String = "backColor"
    Private Const XmlNodeImageIndexAtt As String = "imageindex"

    Private Shared Sub SaveNodes(ByVal nodesCollection As TreeNodeCollection, ByVal textWriter As XmlTextWriter)
        For i As Integer = 0 To nodesCollection.Count - 1
            Dim node As TreeNode = nodesCollection(i)
            textWriter.WriteStartElement(XmlNodeTag)
            textWriter.WriteAttributeString(XmlNodeTextAtt, node.Text)
            textWriter.WriteAttributeString(XmlNodeNameAtt, node.Name)
            textWriter.WriteAttributeString(XmlNodeImageIndexAtt, node.ImageIndex.ToString())
            If Not node.Tag Is Nothing Then
                textWriter.WriteAttributeString(XmlNodeTagAtt, node.Tag.ToString())
                textWriter.WriteAttributeString(XmlNodeForeColorAtt, node.ForeColor.Name)
                textWriter.WriteAttributeString(XmlNodeBackColorAtt, node.BackColor.Name)
                If (node.Nodes.Count > 0) Then
                    SaveNodes(node.Nodes, textWriter)
                End If
            Else
                textWriter.WriteAttributeString(XmlNodeTagAtt, "no tag")
                textWriter.WriteAttributeString(XmlNodeForeColorAtt, node.ForeColor.Name)
                textWriter.WriteAttributeString(XmlNodeBackColorAtt, node.BackColor.Name)
                If (node.Nodes.Count > 0) Then
                    SaveNodes(node.Nodes, textWriter)
                End If
                'MessageBox.Show("no tag")
            End If
            textWriter.WriteEndElement()
        Next
    End Sub

    Private Shared Sub AddTreeViewChildNodes(ByVal parent_nodes As TreeNodeCollection, ByVal xml_node As XmlNode)
        For Each child_node As XmlNode In xml_node.ChildNodes
            ' Make the new TreeView node.
            Dim new_node As TreeNode = parent_nodes.Add(child_node.Attributes("text").Value)
            new_node.Tag = child_node.Attributes("tag").Value
            new_node.Name = child_node.Attributes("name").Value
            new_node.ForeColor = System.Drawing.Color.FromName(child_node.Attributes("foreColor").Value)
            new_node.BackColor = System.Drawing.Color.FromName(child_node.Attributes("backColor").Value)
            ' Recursively make this node's descendants.
            AddTreeViewChildNodes(new_node.Nodes, child_node)
            ' If this is a leaf node, make sure it's visible.
            If new_node.Nodes.Count = 0 Then new_node.EnsureVisible()
        Next child_node
    End Sub

    Public Shared Sub DeserializeTreeView(ByVal treeView As TreeView, ByVal fileName As String)
        Dim xml_doc As New XmlDocument
        xml_doc.Load(fileName)

        ' Add the root node's children to the TreeView.
        treeView.Nodes.Clear()
        AddTreeViewChildNodes(treeView.Nodes, xml_doc.DocumentElement)
    End Sub
End Class

<Serializable()> _
Public Class FileDescription
    Public Sub New()
        nFileInfo = New List(Of FileInfo)
        nInventorInfos = New List(Of InventorInfo)
    End Sub

    Private nFileName As String
    Public Property FileName() As String
        Get
            Return nFileName
        End Get
        Set(ByVal value As String)
            nFileName = value
        End Set
    End Property

    Private nFileInfo As List(Of FileInfo)
    Public Property FileInfo() As List(Of FileInfo)
        Get
            Return nFileInfo
        End Get
        Set(ByVal value As List(Of FileInfo))
            nFileInfo = value
        End Set
    End Property

    Private nInventorInfos As List(Of InventorInfo)
    Public Property InventorInfos() As List(Of InventorInfo)
        Get
            Return nInventorInfos
        End Get
        Set(ByVal value As List(Of InventorInfo))
            nInventorInfos = value
        End Set
    End Property

End Class

<Serializable()> _
Public Class InventorInfo
    Public Sub New()
        nRefDocs = New List(Of RefDoc)
    End Sub

    Private nRefDocs As List(Of RefDoc)
    Public Property RefDocs() As List(Of RefDoc)
        Get
            Return nRefDocs
        End Get
        Set(ByVal value As List(Of RefDoc))
            nRefDocs = value
        End Set
    End Property

End Class

<Serializable()> _
Public Class RefDoc
    Public Sub New(ByVal aName As String, ByVal aPath As String, ByVal imageBitString As String)
        nName = aName
        nPath = aPath
        nImageBitString = imageBitString
    End Sub
    Private nName As String
    Public Property Name() As String
        Get
            Return nName
        End Get
        Set(ByVal value As String)
            nName = value
        End Set
    End Property

    Private nPath As String
    Public Property Path() As String
        Get
            Return nPath
        End Get
        Set(ByVal value As String)
            nPath = value
        End Set
    End Property

    Private nImageBitString As String
    Public Property ImageBitString() As String
        Get
            Return nImageBitString
        End Get
        Set(ByVal value As String)
            nImageBitString = value
        End Set
    End Property

End Class

<Serializable()> _
Public Class DesignVariable

    Private nDesignVariableName As String
    Public Property DesignVariableName() As String
        Get
            Return nDesignVariableName
        End Get
        Set(ByVal value As String)
            nDesignVariableName = value
        End Set
    End Property

    Private nEquation As String
    Public Property Equation() As String
        Get
            Return nEquation
        End Get
        Set(ByVal value As String)
            nEquation = value
        End Set
    End Property

    Private nValue As Single
    Public Property Value() As Single
        Get
            Return nValue
        End Get
        Set(ByVal value As Single)
            nValue = value
        End Set
    End Property

    Private nComment As String
    Public Property Comment() As String
        Get
            Return nComment
        End Get
        Set(ByVal value As String)
            nComment = value
        End Set
    End Property

End Class

<Serializable()> _
Public Class DesignVariableList
    Inherits List(Of DesignVariable)

    Private nDrawingName As String
    Public Property DrawingName() As String
        Get
            Return nDrawingName
        End Get
        Set(ByVal value As String)
            nDrawingName = value
        End Set
    End Property

End Class
