﻿Imports System.Text.RegularExpressions
Imports NativeExcel
Imports System.IO
Imports Autodesk.AutoCAD.Interop
Imports System.Windows.Forms

Public Class ImportItem

    Public Sub New(ByVal aDestFolder As String, aRootPathToRemove As String)
        nAssemblyDrawings = New List(Of String)
        nDestinationFolder = aDestFolder
        nRootPathToRemove = aRootPathToRemove
    End Sub

    Private nDestinationFolder As String
    Private nDrawingName As String
    Public Property DrawingName() As String
        Get
            Return nDrawingName
        End Get
        Set(ByVal value As String)
            nDrawingName = value
        End Set
    End Property

    Private nRootPathToRemove As String
    Public Property RootPathToRemove() As String
        Get
            Return nRootPathToRemove
        End Get
        Set(value As String)
            nRootPathToRemove = value
        End Set
    End Property

    Private ReadOnly Property rootPathToKeep As String
        Get
            Return RootPath.Replace(nRootPathToRemove, "")
        End Get
    End Property

    Private nIsAssembly As Boolean
    Public Property IsAssembly() As Boolean
        Get
            Return nIsAssembly
        End Get
        Set(ByVal value As Boolean)
            nIsAssembly = value
        End Set
    End Property

    Private nIsPart As Boolean
    Public Property IsPart() As Boolean
        Get
            Return nIsPart
        End Get
        Set(ByVal value As Boolean)
            nIsPart = value
        End Set
    End Property

    Private nIsMigrated As Boolean
    Public Property IsMigrated() As String
        Get
            Return nIsMigrated
        End Get
        Set(ByVal value As String)
            nIsMigrated = value
        End Set
    End Property

    Private nRootPath As String
    Public Property RootPath() As String
        Get
            Return nRootPath
        End Get
        Set(ByVal value As String)
            nRootPath = value
        End Set
    End Property

    Private nAssemblyDrawings As List(Of String)
    Public Property AssemblyDrawings() As List(Of String)
        Get
            Return nAssemblyDrawings
        End Get
        Set(ByVal value As List(Of String))
            nAssemblyDrawings = value
        End Set
    End Property

    Private nVolume As Single
    Public Property Volume() As Single
        Get
            Return nVolume
        End Get
        Set(ByVal value As Single)
            nVolume = value
        End Set
    End Property

    Private nSurfaceArea As Single
    Public Property SurfaceArea() As String
        Get
            Return nSurfaceArea
        End Get
        Set(ByVal value As String)
            nSurfaceArea = value
        End Set
    End Property

    Private nFileSize As Integer
    Public Property FileSize() As Integer
        Get
            Return nFileSize
        End Get
        Set(ByVal value As Integer)
            nFileSize = value
        End Set
    End Property

    Public ReadOnly Property DrawingPath()
        Get
            Return nRootPath & "\" & nDrawingName & ".dwg"
        End Get
    End Property

    Public ReadOnly Property DestinationPath()
        Get
            Dim rootPathAddition As String
            If nRootPathToRemove = "" Then
                rootPathAddition = ""
            Else
                rootPathAddition = rootPathToKeep
            End If
            If IsAssembly = True Then
                Return nDestinationFolder & rootPathAddition & "\" & nDrawingName & ".iam"
            ElseIf IsPart = True Then
                Return nDestinationFolder & rootPathAddition & "\" & DrawingName & ".ipt"
            Else
                Return nDestinationFolder & rootPathAddition & "\" & DrawingName & ".dwg"
            End If
        End Get
    End Property

    Public ReadOnly Property IsMdtFile() As Boolean
        Get
            If nIsAssembly = False And IsPart = False Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property

    Public Function readyToMigrate(iItem As ImportItem, iItems As ImportItems) As Boolean
        For Each itmName As String In iItem.AssemblyDrawings
            iItems.PathToFind = iItem.DrawingPath
            Dim itm As ImportItem = iItems.Find(AddressOf iItems.FindByPath)
            If Not itm Is Nothing Then
                For Each assmItm As String In itm.AssemblyDrawings
                    iItems.PathToFind = assmItm
                    Dim itm2 As ImportItem = iItems.Find(AddressOf iItems.FindByPath)
                    If Not itm2 Is Nothing Then
                        If itm2.IsMigrated = False Then Return False
                    End If
                Next
            End If
        Next
        Return True
    End Function

End Class

Public Class ImportItems
    Inherits List(Of ImportItem)

    Private Function convertToLocal(ByVal fname As String) As String
        'create temp directory
        Try
            If Not Directory.Exists("c:\dwgTemp") Then Directory.CreateDirectory("c:\dwgTemp")
            Dim localName As String = "c:\dwgTemp\" & Path.GetFileName(fname)
            File.Copy(fname, localName, True)
            File.SetAttributes(localName, FileAttributes.Normal)
            Return localName
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show("The file may be open in inventor or MDT please close the file and press 'OK'")
            Try
                If Not Directory.Exists("c:\dwgTemp") Then Directory.CreateDirectory("c:\dwgTemp")
                Dim localName As String = "c:\dwgTemp\" & Path.GetFileName(fname)
                File.Copy(fname, localName, True)
                File.SetAttributes(localName, FileAttributes.Normal)
                Return localName
            Catch ex1 As Exception
                Return ""
            End Try
        End Try
    End Function

    Public Sub New(sourceFolder As String, destFolder As String, oMdtApp As Autodesk.AutoCAD.Interop.AcadApplication, chkCopyLocal As Boolean, rootPathToRemove As String, rtxt As Windows.Forms.RichTextBox)
        'this will be a two step process analyze the files and then import them
        Dim files() As String = System.IO.Directory.GetFiles(sourceFolder, "*.dwg", IO.SearchOption.AllDirectories)
        For Each fle As String In files
            Dim dwg As Autodesk.AutoCAD.Interop.AcadDocument
            If chkCopyLocal = True Then
                'set up local
                Dim localDwg As String = convertToLocal(fle)
                Try
                    dwg = oMdtApp.Documents.Open(localDwg)
                    setImportInfo(dwg, destFolder, fle, rootPathToRemove)
                    dwg.Close(False)
                    File.Delete(localDwg)
                Catch ex As Runtime.InteropServices.COMException
                    rtxt.Text += ex.Message & vbCrLf
                End Try
            Else
                Try
                    dwg = oMdtApp.Documents.Open(fle)
                    setImportInfo(dwg, destFolder, fle, rootPathToRemove)
                    dwg.Close(False)
                Catch ex As Runtime.InteropServices.COMException
                    rtxt.Text += ex.Message & vbCrLf
                End Try
            End If
        Next
    End Sub

    Private Sub setProps(importItm As ImportItem, oDwg As Autodesk.AutoCAD.Interop.AcadDocument)
        Try
            Dim mProps As MassProp = getMassProps(oDwg)
            importItm.SurfaceArea = mProps.SurfaceArea
            importItm.Volume = mProps.Volume
        Catch ex As Exception
            importItm.SurfaceArea = 0
            importItm.Volume = 0
        End Try
    End Sub

    Private Sub setImportInfo(dwg As Autodesk.AutoCAD.Interop.AcadDocument, destFolder As String, fle As String, rootPathToRemove As String)
        If Not dwg Is Nothing Then
            Dim aImportItem As New ImportItem(destFolder, rootPathToRemove)
            setProps(aImportItem, dwg)
            aImportItem.FileSize = New FileInfo(fle).Length / 1024
            aImportItem.RootPath = System.IO.Path.GetDirectoryName(fle)
            aImportItem.DrawingName = System.IO.Path.GetFileNameWithoutExtension(fle)
            aImportItem.IsMigrated = False
            If isAssembly(dwg) = True Then
                aImportItem.IsAssembly = True
            Else
                aImportItem.IsAssembly = False
                If isPart(dwg) Then
                    aImportItem.IsPart = True
                Else
                    aImportItem.IsPart = False
                End If
            End If
            Me.Add(aImportItem)
        End If
    End Sub

    Public Sub New(ByVal xlsFile As String, ByVal destFolder As String, rootPathToRemove As String)
        If System.IO.File.Exists(xlsFile) Then
            Dim aWrkBk As IWorkbook = NativeExcel.Factory.OpenWorkbook(xlsFile)
            Dim aWrkSt As IWorksheet = aWrkBk.Worksheets(1)
            aWrkSt.Activate()
            Dim rootPath As String = Regex.Split(aWrkSt.Cells(2, 1).Value, ":")(1)
            Dim aImportItem As New ImportItem(destFolder, rootPathToRemove)
            For i As Integer = 4 To aWrkSt.UsedRange.Rows.Count
                If Not aWrkSt.Cells(i, 1).Value Is Nothing Then
                    If Not i = 4 Then Me.Add(aImportItem)
                    aImportItem = New ImportItem(destFolder, rootPathToRemove)
                    aImportItem.RootPath = rootPath.Trim
                    Dim drawingName() As String = Regex.Split(aWrkSt.Cells(i, 1).Value, "\.")
                    aImportItem.DrawingName = drawingName(0)
                    aImportItem.IsMigrated = False
                    If aWrkSt.Cells(i, 2).Value = "no" Then
                        aImportItem.IsAssembly = False
                    Else
                        aImportItem.IsAssembly = True
                    End If
                    If aWrkSt.Cells(i, 3).Value = "yes" Then
                        aImportItem.IsPart = True
                    Else
                        aImportItem.IsPart = False
                    End If
                Else
                    'this may be an assembly item
                    If Not aWrkSt.Cells(i, 4).Value Is Nothing Then
                        Dim assmItem As String = Regex.Split(aWrkSt.Cells(i, 4).Value, "_")(0)
                        If Not aImportItem.AssemblyDrawings.Contains(assmItem) Then aImportItem.AssemblyDrawings.Add(assmItem)
                    End If
                End If
            Next
        End If
    End Sub

    Public Function isPart(ByVal aDwg As AcadDocument) As Boolean
        Try
            aDwg.SendCommand("ammode m amactivate" & vbCr & vbCr & Chr(27))
            aDwg.SendCommand("ammode m amlistpart c" & vbCr & Chr(27))
            aDwg.SendCommand("ammode m amlistpart a " & vbCr & Chr(27))
            aDwg.SendCommand("copyhist " & Chr(27))
            Dim strClip As String = GetClipboardText()
            Dim histLines As New List(Of String)
            If Not strClip Is Nothing Then
                Dim split() As String = strClip.Split(vbCrLf.ToCharArray())
                Dim ct As Integer = 0
                For Each Str As String In split
                    If Str <> "" Then
                        histLines.Add(Str)
                    End If
                    ct += 1
                Next
                For Each strHistLine As String In histLines
                    If strHistLine.ToLower.Contains("adpart") Then
                        Return True
                        Exit For
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
            Return False
        End Try
    End Function

    Public Function isAssembly(aDwg As AcadDocument) As Boolean
        Dim blnIsAssembly As Boolean = False
        Try
            aDwg.SetVariable("expert", 5)
            aDwg.SetVariable("proxynotice", 0)
            aDwg.SendCommand("ammode m amactivate" & vbCr & vbCr & Chr(27))
            aDwg.SendCommand("ammode m amlistassm name *" & vbCr & Chr(27))
            aDwg.SendCommand("ammode m amlistpart c" & vbCr & Chr(27))
            aDwg.SendCommand("ammode m amlistpart a " & vbCr & Chr(27))
            aDwg.SendCommand("-view ? " & vbCr & vbCrLf & Chr(27))
            aDwg.SendCommand("copyhist ")
            'see if any assembly or part information has been collected
            Dim strClip As String = GetClipboardText()
            Dim histLines As New List(Of String)
            If Not strClip Is Nothing Then
                Dim split() As String = strClip.Split(vbCrLf.ToCharArray())
                Dim ct As Integer = 0
                For Each Str As String In split
                    If Str <> "" Then
                        histLines.Add(Str)
                    End If
                    ct += 1
                Next
                For Each strLine As String In histLines
                    'preload information
                    If strLine.ToLower.Contains("part/subassembly name:") Then
                        blnIsAssembly = True
                        Exit For
                    End If
                Next
            End If
            Return blnIsAssembly
        Catch ex As Exception
            Return blnIsAssembly
        End Try
    End Function

    Public Function GetClipboardText() As String
        Try
            Dim objClipboard As IDataObject = Clipboard.GetDataObject()
            With objClipboard
                If .GetDataPresent(DataFormats.Text) Then
                    Return .GetData(DataFormats.Text)
                Else
                    Return ""
                End If
            End With
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function getMassProps(ByVal aDwg As AcadDocument) As MassProp
        Dim tmpMassProp As New MassProp
        If isPart(aDwg) Or isAssembly(aDwg) Then
            Dim strTmpMass As String = "C:\tmpMass.mpr"
            If File.Exists(strTmpMass) Then File.Delete(strTmpMass)

            aDwg.SetVariable("expert", 5)
            aDwg.SetVariable("proxynotice", 0)
            aDwg.SetVariable("filedia", 0)
            aDwg.SendCommand(Chr(27))
            aDwg.SendCommand("ammode m ")
            aDwg.SendCommand("-ammassprop all" & vbCr & vbCr & "cg" & vbCr & "i" & vbCr & "4" & vbCr & strTmpMass & vbCr & Chr(27))
            aDwg.SendCommand("commandline " & Chr(27))
            'open the text file for reading
            Dim strMassProps As New List(Of String)
            Using sr As New StreamReader(strTmpMass)
                Do Until sr.EndOfStream
                    strMassProps.Add(sr.ReadLine)
                Loop
            End Using
            'Dim strRdr As StreamReader = My.Computer.FileSystem.OpenTextFileReader(strTmpMass)
            'While Not strRdr.EndOfStream
            '    strMassProps.Add(strRdr.ReadLine)
            'End While
            Dim count As Integer = 0
            tmpMassProp.DrawingName = aDwg.Name
            For Each strMassLine As String In strMassProps
                If strMassLine.Contains("Summary:") Then
                    'the next three lines contain mass, volume and surface area
                    Dim splitMass() As String = Regex.Split(strMassProps(count + 1), " ")
                    tmpMassProp.Mass = splitMass(splitMass.Length - 2)
                    Dim splitVolume() As String = Regex.Split(strMassProps(count + 2), " ")
                    tmpMassProp.Volume = splitVolume(splitVolume.Length - 2)
                    Dim splitSurface() As String = Regex.Split(strMassProps(count + 3), " ")
                    tmpMassProp.SurfaceArea = splitSurface(splitSurface.Length - 2)
                End If
                If strMassLine.Contains("Mass moments of inertia") Then
                    'the next three line contain X, Y and Z moments
                    Dim splitX() As String = Regex.Split(strMassProps(count + 1), " ")
                    tmpMassProp.MomentX = splitX(splitX.Length - 3)
                    Dim splitY() As String = Regex.Split(strMassProps(count + 2), " ")
                    tmpMassProp.MomentY = splitY(splitY.Length - 3)
                    Dim splitZ() As String = Regex.Split(strMassProps(count + 3), " ")
                    tmpMassProp.MomentZ = splitZ(splitZ.Length - 3)
                End If
                count += 1
            Next
        End If
        Return tmpMassProp
    End Function

    Private nPathToFind As String
    Public WriteOnly Property PathToFind() As String
        Set(ByVal value As String)
            nPathToFind = value
        End Set
    End Property

    Public Function getPartFiles() As List(Of ImportItem)
        Dim qry = From c In Me Where c.IsPart = True Select c

        Dim tmpList As New List(Of ImportItem)
        For Each impItm As ImportItem In qry
            tmpList.Add(impItm)
        Next
        Return tmpList
    End Function

    Public Function getAssemblyFiles() As List(Of ImportItem)
        Dim qry = From c In Me Where c.IsAssembly = True Select c

        Dim tmpList As New List(Of ImportItem)
        For Each impItm As ImportItem In qry
            tmpList.Add(impItm)
        Next
        Return tmpList
    End Function

    Public Function getNonMdtFiles() As List(Of ImportItem)
        Dim qry = From c In Me Where c.IsMdtFile = False Select c

        Dim tmpList As New List(Of ImportItem)
        For Each impItm As ImportItem In qry
            tmpList.Add(impItm)
        Next
        Return tmpList
    End Function

    Public Function MoveToTempLocation(ByVal tempFolder As String, ByVal iItem As ImportItem) As Boolean
        Try
            If Not System.IO.Directory.Exists(tempFolder) Then System.IO.Directory.CreateDirectory(tempFolder)
            Dim destPath As String = tempFolder & "\" & System.IO.Path.GetFileName(iItem.DrawingPath)
            System.IO.File.Copy(iItem.DrawingPath, destPath, True)
            System.IO.File.SetAttributes(destPath, IO.FileAttributes.Normal)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function FindByPath(ByVal iItem As ImportItem) As Boolean
        If iItem.DrawingPath = nPathToFind Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function readyToMigrate(iItem As ImportItem) As Boolean
        For Each itmName As String In iItem.AssemblyDrawings
            Dim itm As ImportItem = FindByName(itmName)
            If Not itm Is Nothing Then
                If itm.IsMigrated = False Then
                    Return False
                End If
            End If
        Next
        Return True
    End Function

    Private Function FindByName(Name As String) As ImportItem
        For Each itm As ImportItem In Me
            If itm.DrawingName = Name Then
                Return itm
            End If
        Next
        Return Nothing
    End Function

End Class

<Serializable()> _
Public Class MassProp
    Implements IComparable

    Public Sub New()
    End Sub

    Public Sub New(ByVal aName As String, ByVal aMass As Single, ByVal aVolume As Single, ByVal aSurface As Single, ByVal aMX As Single, ByVal aMY As Single, ByVal aMZ As Single)
        nDrawingName = aName
        nMass = aMass
        nVolumne = aVolume
        nSurfaceArea = aSurface
        nMmiX = aMX
        nMmiY = aMY
        nMmiZ = aMZ
    End Sub

    Public Sub New(ByVal aName As String, ByVal aMass As Single, ByVal aVolume As Single, ByVal aSurface As Single, ByVal aMX As Single, ByVal aMY As Single, ByVal aMZ As Single, ByVal aDesingVars As DesignVariableList)
        nDrawingName = aName
        nMass = aMass
        nVolumne = aVolume
        nSurfaceArea = aSurface
        nMmiX = aMX
        nMmiY = aMY
        nMmiZ = aMZ
        nDesignVariables = aDesingVars
    End Sub

    Private nSuccess As Boolean
    Public Property Success() As Boolean
        Get
            Return nSuccess
        End Get
        Set(ByVal value As Boolean)
            nSuccess = value
        End Set
    End Property

    Private nDrawingName As String
    Public Property DrawingName() As String
        Get
            Return nDrawingName
        End Get
        Set(ByVal value As String)
            nDrawingName = value
        End Set
    End Property

    Private nMass As Single
    Public Property Mass() As Single
        Get
            Return nMass
        End Get
        Set(ByVal value As Single)
            nMass = value
        End Set
    End Property

    Private nVolumne As Single
    Public Property Volume() As Single
        Get
            Return nVolumne
        End Get
        Set(ByVal value As Single)
            nVolumne = value
        End Set
    End Property

    Private nSurfaceArea As Single
    Public Property SurfaceArea() As Single
        Get
            Return nSurfaceArea
        End Get
        Set(ByVal value As Single)
            nSurfaceArea = value
        End Set
    End Property

    Private nMmiX As Double
    Public Property MomentX() As Double
        Get
            Return nMmiX
        End Get
        Set(ByVal value As Double)
            nMmiX = value
        End Set
    End Property

    Private nMmiY As Double
    Public Property MomentY() As Double
        Get
            Return nMmiY
        End Get
        Set(ByVal value As Double)
            nMmiY = value
        End Set
    End Property

    Private nMmiZ As Double
    Public Property MomentZ() As Double
        Get
            Return nMmiZ
        End Get
        Set(ByVal value As Double)
            nMmiZ = value
        End Set
    End Property

    Private nDesignVariables As DesignVariableList
    Public Property DesignVariables() As DesignVariableList
        Get
            Return nDesignVariables
        End Get
        Set(ByVal value As DesignVariableList)
            nDesignVariables = value
        End Set
    End Property

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        If obj Is Nothing Then Return 1
        Dim other As MassProp = DirectCast(obj, MassProp)
        Dim blnTheSame As Boolean = True
        Dim otherRootName() As String = Regex.Split(other.nDrawingName, "_")
        Dim currentRootName() As String = Regex.Split(nDrawingName, "_")
        If otherRootName(0) <> currentRootName(0) Then
            Return 1
        End If
        If other.nMass <> nMass Then
            Return 1
        End If
        If other.nVolumne <> nVolumne Then
            Return 1
        End If
        If other.nSurfaceArea <> nSurfaceArea Then
            Return 1
        End If
        If other.nMmiX <> nMmiX Then
            Return 1
        End If
        If other.nMmiY <> nMmiY Then
            Return 1
        End If
        If other.nMmiZ <> nMmiZ Then
            Return 1
        End If
        'we have run through all the comparisons and the mass properties are the same
        Return 0
    End Function
End Class
